package com.example.projetandroidrestoliste;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class new_restaurant extends AppCompatActivity {

    public static final String EXTRA_REPLY = "com.example.android.wordlistsql.REPLY_NOM";
    public static final String EXTRA_REPLY_ADRESSE = "com.example.android.wordlistsql.REPLY_ADRESSE";
    public static final String EXTRA_REPLY_DESCRIPTION = "com.example.android.wordlistsql.REPLY_DESCRIPTION";
    public static final String EXTRA_REPLY_LISTCATEGORIE = "com.example.android.wordlistsql.REPLY_LISTCATEGORIE";

    private EditText editRestaurantView;
    private EditText editRestaurantAdresseView;
    private EditText editRestaurantDescriptionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_restaurant);


        editRestaurantView = findViewById(R.id.edit_restaurant);
        editRestaurantAdresseView = findViewById(R.id.edit_adresse);
        editRestaurantDescriptionView = findViewById(R.id.edit_descritption);


        final CheckBox checkboxFastFood = findViewById(R.id.checkbox_fastfood);
        final CheckBox checkboxGastronomique = findViewById(R.id.checkbox_gastronomique);
        final CheckBox checkboxPizzeria = findViewById(R.id.checkbox_pizzeria);
        final CheckBox checkboxGrill = findViewById(R.id.checkbox_grill);
        final CheckBox checkboxBuffet = findViewById(R.id.checkbox_buffet_a_volonte);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(view -> {

            Intent replyIntent = new Intent();

            if (TextUtils.isEmpty(editRestaurantView.getText()) || TextUtils.isEmpty(editRestaurantAdresseView.getText()) || TextUtils.isEmpty(editRestaurantDescriptionView.getText())) {
                setResult(RESULT_CANCELED, replyIntent);
            } else {
                String restaurantNom = editRestaurantView.getText().toString();
                String restaurantAdresse = editRestaurantAdresseView.getText().toString();
                String restaurantDescription = editRestaurantDescriptionView.getText().toString();

                boolean bool = false;
                List<String> listCatChecked = new ArrayList<>();

                if (checkboxFastFood.isChecked()) {
                    Log.d("Checkbox", checkboxFastFood.getText().toString());
                    listCatChecked.add(checkboxFastFood.getText().toString());
                    bool = true;
                }
                if (checkboxGastronomique.isChecked()) {
                    Log.d("Checkbox", checkboxGastronomique.getText().toString());
                    listCatChecked.add(checkboxGastronomique.getText().toString());
                    bool = true;
                }
                if (checkboxPizzeria.isChecked()) {
                    Log.d("Checkbox", checkboxPizzeria.getText().toString());
                    listCatChecked.add(checkboxPizzeria.getText().toString());
                    bool = true;
                }
                if (checkboxGrill.isChecked()) {
                    Log.d("Checkbox", checkboxGrill.getText().toString());
                    listCatChecked.add(checkboxGrill.getText().toString());
                    bool = true;
                }
                if (checkboxBuffet.isChecked()) {
                    Log.d("Checkbox", checkboxBuffet.getText().toString());
                    listCatChecked.add(checkboxBuffet.getText().toString());
                    bool = true;
                }
                if(bool == false){
                    Log.d("Checkbox", "Aucune checkbox de cochée");
                }

                Log.d("Etat de bool : ", String.valueOf(bool));

                if(listCatChecked.isEmpty()){
                    Log.d("Ma listCatChecked", "est vide");
                }
                else{
                    Log.d("Ma listCatChecked", String.valueOf(listCatChecked));
                }

                ArrayList<String> listCatCheckedSerializable = new ArrayList<>(listCatChecked);

                replyIntent.putExtra(EXTRA_REPLY, restaurantNom);
                replyIntent.putExtra(EXTRA_REPLY_ADRESSE, restaurantAdresse);
                replyIntent.putExtra(EXTRA_REPLY_DESCRIPTION, restaurantDescription);

                replyIntent.putStringArrayListExtra(EXTRA_REPLY_LISTCATEGORIE, listCatCheckedSerializable);

                setResult(RESULT_OK, replyIntent);

            }
            finish();
        });
    }
}