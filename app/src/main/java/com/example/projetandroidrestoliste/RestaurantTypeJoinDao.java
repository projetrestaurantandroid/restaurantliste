package com.example.projetandroidrestoliste;

import androidx.annotation.IntegerRes;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface RestaurantTypeJoinDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertRestaurantType(RestaurantTypeJoin restaurantTypeJoin);

    @Delete
    void deleteRestaurantTypeJoin(RestaurantTypeJoin restaurantTypeJoin);

    @Query("SELECT * FROM restaurant_type_join WHERE restaurantId = :restaurantId")
    LiveData<List<RestaurantTypeJoin>> getRestaurantTypes(int restaurantId);

    @Query("SELECT * FROM restaurant_type_join")
    LiveData<List<RestaurantTypeJoin>> getAllRestaurantTypes();

    @Query("DELETE FROM restaurant_type_join WHERE restaurantId = :restaurantId AND typeId = :typeId")
    void deleteByRestaurantAndType(int restaurantId, int typeId);

    @Query("SELECT COUNT(*) FROM restaurant_type_join WHERE restaurantId = :restaurantId AND typeId = :typeId")
    LiveData<Integer> getCount(int restaurantId, int typeId);

    @Query("SELECT typeId FROM restaurant_type_join WHERE restaurantId = :restaurantID")
    LiveData<Integer> getTypeIdByRestaurantId(int restaurantID);

    /*@Query("SELECT * FROM restaurant_type_join WHERE restaurantId = :restaurantId AND typeId = :typeId")
    RestaurantTypeJoin exists(int restaurantId, int typeId);*/
}
