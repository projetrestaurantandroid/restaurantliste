package com.example.projetandroidrestoliste;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import kotlin.OverloadResolutionByLambdaReturnType;

public class RestaurantRepository {

    private RestaurantDao restaurantDao;
    private LiveData<List<Restaurant>> allRestaurants;
    private LiveData<Integer> nbRestaurants;

    public RestaurantRepository(Application application) {
        RestaurantRoomDatabase database = RestaurantRoomDatabase.getDatabase(application);
        restaurantDao = database.restaurantDao();
        allRestaurants = restaurantDao.getAllRestaurants();
        nbRestaurants = restaurantDao.getNbRestaurant();
    }


    private static MutableLiveData<Long> restaurantId = new MutableLiveData<>();
    public MutableLiveData<Long> getRestaurantId() {
        return restaurantId;
    }
    public void insertRestaurant(Restaurant restaurant) {
        new InsertRestaurantAsyncTask(restaurantDao).execute(restaurant);
    }
    private static class InsertRestaurantAsyncTask extends AsyncTask<Restaurant, Void, Long> {
        private RestaurantDao restaurantDao;

        private InsertRestaurantAsyncTask(RestaurantDao restaurantDao) {
            this.restaurantDao = restaurantDao;
        }

        @Override
        protected Long doInBackground(Restaurant... restaurants) {
            long id = restaurantDao.insertRestaurant(restaurants[0]);
            return id;
        }

        @Override
        protected void onPostExecute(Long id) {
            super.onPostExecute(id);
            restaurantId.postValue(id);
        }
    }


    public void updateRestaurant(Restaurant restaurant) {
        new UpdateRestaurantAsyncTask(restaurantDao).execute(restaurant);
    }

    public void deleteRestaurant(Restaurant restaurant) {
        new DeleteRestaurantAsyncTask(restaurantDao).execute(restaurant);
    }

    public void deleteAllRestaurants() {
        new DeleteAllRestaurantsAsyncTask(restaurantDao).execute();
    }

    public LiveData<List<Restaurant>> getRestaurantById(int restaurantId){
        return restaurantDao.getRestaurantById(restaurantId);
    }

    public LiveData<List<Restaurant>> getAllRestaurants() {
        return allRestaurants;
    }

    public LiveData<Integer> getNbRestaurants(){ return nbRestaurants; }

    public LiveData<Integer> getRestaurantIdByNameAddressDescription(String name, String address, String description){
        return restaurantDao.getRestaurantIdByNameAddressDescription(name, address, description);
    }




    private static class UpdateRestaurantAsyncTask extends AsyncTask<Restaurant, Void, Void> {
        private RestaurantDao restaurantDao;

        private UpdateRestaurantAsyncTask(RestaurantDao restaurantDao) {
            this.restaurantDao = restaurantDao;
        }

        @Override
        protected Void doInBackground(Restaurant... restaurants) {
            restaurantDao.updateRestaurant(restaurants[0]);
            return null;
        }
    }

    private static class DeleteRestaurantAsyncTask extends AsyncTask<Restaurant, Void, Void> {
        private RestaurantDao restaurantDao;

        private DeleteRestaurantAsyncTask(RestaurantDao restaurantDao) {
            this.restaurantDao = restaurantDao;
        }

        @Override
        protected Void doInBackground(Restaurant... restaurants) {
            restaurantDao.deleteRestaurant(restaurants[0]);
            return null;
        }
    }

    private static class DeleteAllRestaurantsAsyncTask extends AsyncTask<Void, Void, Void> {
        private RestaurantDao restaurantDao;

        private DeleteAllRestaurantsAsyncTask(RestaurantDao restaurantDao) {
            this.restaurantDao = restaurantDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            restaurantDao.deleteAllRestaurants();
            return null;
        }
    }

    /*private RestaurantDao restaurantDao;
    private MutableLiveData<Long> insertedRestoId = new MutableLiveData<>();
    private LiveData<List<Restaurant>> allMyRestaurants;
    private LiveData<Integer> nbRestaurantsLD;



    private TypeRestaurantDao typeRestaurantDao;
    private MutableLiveData<Long> insertedTypeId = new MutableLiveData<>();
    private LiveData<List<TypeRestaurant>> allRestaurantsTypes;
    private LiveData<Integer> nbRestaurantsTypesLD;




    private RestaurantTypeJoinDao restaurantTypeJoinDao;
    private LiveData<List<RestaurantTypeJoin>> allMyRestaurantsByTypes;
    private LiveData<List<RestaurantTypeJoin>> allMyTypesByRestaurant;
    private LiveData<List<Restaurant>> allRestaurantsForTypes;
    private LiveData<List<TypeRestaurant>> allTypesForRestaurants;




    RestaurantRepository(Application application){
        RestaurantRoomDatabase db = RestaurantRoomDatabase.getDatabase(application);

        restaurantDao = db.restaurantDao();

        allMyRestaurants = restaurantDao.getAllRestaurantsLD();
        nbRestaurantsLD = restaurantDao.getNbRestaurantLD();

        typeRestaurantDao = db.typeRestaurantDao();

        allRestaurantsTypes = typeRestaurantDao.getAllRestaurantsTypeLD();
        nbRestaurantsTypesLD = typeRestaurantDao.getNbRestaurantsTypeLD();

        restaurantTypeJoinDao = db.restaurantTypeJoinDao();
    }

    LiveData<List<RestaurantTypeJoin>> getAllMyRestaurantsByTypes(int typeId){
        return allMyRestaurantsByTypes;
    }
    LiveData<List<RestaurantTypeJoin>> getAllMyTypesByRestaurant(int restaurantId){
        return allMyTypesByRestaurant;
    }
    LiveData<List<Restaurant>> getRestaurantsForTypes(final int typeId){
        return allRestaurantsForTypes;
    }
    LiveData<List<TypeRestaurant>> getTypesForRestaurants(final int restoId){
        return allTypesForRestaurants;
    }
    void insert(RestaurantTypeJoin restaurantTypeJoin){
        RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            restaurantTypeJoinDao.insert(restaurantTypeJoin);
        });
    }
    void delete(RestaurantTypeJoin restaurantTypeJoin){
        RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            restaurantTypeJoinDao.delete(restaurantTypeJoin);
        });
    }

    LiveData<List<Restaurant>> getAllMyRestaurants(){
        return allMyRestaurants;
    }
    LiveData<Integer> getNbRestaurantsLD(){
        return nbRestaurantsLD;
    }
    LiveData<String> getAdresseRestaurant(String nomResto){
        return restaurantDao.getAdresseByNom(nomResto);
    }
    LiveData<Integer> getLastRestaurantId(){
        return restaurantDao.getLastRestaurantId();
    }


    void delete(Restaurant restaurant){
        RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            restaurantDao.delete(restaurant);
        });
    }
    void deleteAll(){
        RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            restaurantDao.deleteAll();
        });
    }
    void updateInfosResto(Restaurant restaurant){
        RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            restaurantDao.updateInfosResto(restaurant);
        });
    }



    public LiveData<Long> getInsertedIdType(){
        return insertedTypeId;
    }
    void insertType(TypeRestaurant typeRestaurant){
        *//*RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            typeRestaurantDao.insert(typeRestaurant);
        });*//*
        new insertAsyncTask(typeRestaurantDao).execute(typeRestaurant);
    }
    public LiveData<Long> getInsertedIdResto(){
        return insertedRestoId;
    }
    void insertRestaurant(Restaurant restaurant){
        *//*RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            restaurantDao.insert(restaurant);
        });*//*
        new insertAsyncTaskResto(restaurantDao).execute(restaurant);
    }

    LiveData<List<TypeRestaurant>> getAllRestaurantsTypes(){
        return allRestaurantsTypes;
    }
    LiveData<Integer> getNbRestaurantsTypesLD(){
        return nbRestaurantsTypesLD;
    }
    LiveData<Integer>  getLastRestaurantTypeId(){
        return typeRestaurantDao.getLastRestaurantTypeId();
    }

    void delete(TypeRestaurant typeRestaurant){
        RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            typeRestaurantDao.delete(typeRestaurant);
        });
    }
    void deleteAllTypes(){
        RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            typeRestaurantDao.deleteAllTypes();
        });
    }
    void updateInfosRestoType(TypeRestaurant typeRestaurant){
        RestaurantRoomDatabase.databaseWriteExecutor.execute(() -> {
            typeRestaurantDao.updateInfosRestoType(typeRestaurant);
        });
    }

    private class insertAsyncTaskResto extends AsyncTask<Restaurant, Void, Long>{
        private RestaurantDao mAsyncTaskDao;

        insertAsyncTaskResto(RestaurantDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Long doInBackground(final Restaurant... params){
            return mAsyncTaskDao.insert(params[0]);
        }
        @Override
        protected void onPostExecute(Long result){
            super.onPostExecute(result);
            insertedRestoId.setValue(result);
        }
    }

    private class insertAsyncTask extends AsyncTask<TypeRestaurant, Void, Long>{
        private TypeRestaurantDao mAsyncTaskDaoType;

        insertAsyncTask(TypeRestaurantDao daoType){
            mAsyncTaskDaoType = daoType;
        }

        @Override
        protected Long doInBackground(final TypeRestaurant... params){
            return mAsyncTaskDaoType.insert(params[0]);
        }
        @Override
        protected void onPostExecute(Long result){
            super.onPostExecute(result);
            insertedTypeId.setValue(result);
        }
    }*/

}
