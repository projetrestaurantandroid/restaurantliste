package com.example.projetandroidrestoliste;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TypeRestaurantDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE )
    long insertType(TypeRestaurant typeRestaurant);

    @Update
    void updateType(TypeRestaurant typeRestaurant);

    @Delete
    void deleteType(TypeRestaurant typeRestaurant);

    @Query("DELETE FROM table_typeRestaurant")
    void deleteAllTypes();

    @Query("SELECT * FROM table_typeRestaurant")
    LiveData<List<TypeRestaurant>> getAllTypes();

    @Query("SELECT type FROM table_typeRestaurant WHERE idType = :idType")
    LiveData<String> getTypeById(int idType);

    @Query("SELECT idType FROM table_typeRestaurant WHERE type = :nomType")
    LiveData<Integer> getIdByTypeName(String nomType);


}
