package com.example.projetandroidrestoliste;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.w3c.dom.Text;

import java.io.Console;
import java.util.List;

public class Consultation extends AppCompatActivity {

    TextView tvNomResto;
    TextView tvAdressResto;
    TextView tvDescriptionResto;
    TextView tvCategorieResto;

    String nomResto;
    String adresseResto;
    String descriptionResto;

    private RestaurantViewModel consultationViewModel;


    public void retour(View view){
        Intent intent = new Intent(view.getContext(), MainActivity.class);
        startActivity(intent);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultation);


        consultationViewModel = new ViewModelProvider(this).get(RestaurantViewModel.class);

        //LiveData<Integer> idRestaurant;


        tvNomResto = findViewById(R.id.tvNomResto);
        tvAdressResto = findViewById(R.id.tvAdresseResto);
        tvDescriptionResto = findViewById(R.id.tvDescriptionResto);
        tvCategorieResto = findViewById(R.id.tvCategorie);


        Intent intent = getIntent();
        nomResto = intent.getStringExtra("nomResto");
        adresseResto = intent.getStringExtra("adresseResto");
        descriptionResto = intent.getStringExtra("descriptionResto");


        ImageButton deleteResto = findViewById(R.id.button3);

        deleteResto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("ALERT", "Bouton delete cliqué !");

                consultationViewModel.getRestaurantIdByNameAddressDescription(nomResto, adresseResto, descriptionResto).observe(Consultation.this, new Observer<Integer>() {
                    @Override
                    public void onChanged(Integer integer) {
                        consultationViewModel.getRestaurantById(integer).observe(Consultation.this, new Observer<List<Restaurant>>() {
                            @Override
                            public void onChanged(List<Restaurant> restaurants) {
                                Log.d("ALERT", String.valueOf(restaurants.size()));
                                consultationViewModel.deleteRestaurant(restaurants.get(0));

                                consultationViewModel.getRestaurantTypes(integer).observe(Consultation.this, new Observer<List<RestaurantTypeJoin>>() {
                                    @Override
                                    public void onChanged(List<RestaurantTypeJoin> restaurantTypeJoins) {
                                        for(int i = 0; i<restaurantTypeJoins.size(); i++){
                                            consultationViewModel.deleteRestaurantTypeJoin(restaurantTypeJoins.get(i));
                                        }
                                    }
                                });
                            }
                        });
                    }
                });

            }
        });




        consultationViewModel.getRestaurantIdByNameAddressDescription(nomResto, adresseResto, descriptionResto).observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer idResto) {
                Log.d("IDRESTO", String.valueOf(idResto));

                consultationViewModel.getRestaurantTypes(idResto).observe(Consultation.this, new Observer<List<RestaurantTypeJoin>>() {
                    @Override
                    public void onChanged(List<RestaurantTypeJoin> restaurantTypeJoins) {

                        final String[] machaine = {""};

                        for(int i = 0; i < restaurantTypeJoins.size(); i++){
                            Log.d("ID_TYPE_IN_JOIN", String.valueOf(restaurantTypeJoins.get(i).getTypeId()));

                            consultationViewModel.getTypeById(restaurantTypeJoins.get(i).getTypeId()).observe(Consultation.this, new Observer<String>() {
                                @Override
                                public void onChanged(String s) {
                                    Log.d("Le type associé", s);
                                    machaine[0] += " "+s;
                                    tvCategorieResto.setText("Catégorie(s) du restaurant : "+ machaine[0]);
                                }
                            });

                        }

                    }
                });

                /*consultationViewModel.getTypeIdByRestaurantId(idResto).observe(Consultation.this, new Observer<Integer>() {
                    @Override
                    public void onChanged(Integer idType) {
                        Log.d("IDTYPE", String.valueOf(idType));

                        consultationViewModel.getTypeById(idType).observe(Consultation.this, new Observer<String>() {
                            @Override
                            public void onChanged(String s) {
                                Log.d("MESTYPES", s);
                                tvCategorieResto.setText("Catégorie(s) du restaurant : "+ s);
                            }
                        });
                    }
                });*/

            }
        });



        tvNomResto.setText(nomResto);
        tvAdressResto.setText(adresseResto);
        tvDescriptionResto.setText(descriptionResto);
    }
}