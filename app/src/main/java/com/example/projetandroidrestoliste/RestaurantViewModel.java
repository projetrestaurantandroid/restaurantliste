package com.example.projetandroidrestoliste;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class RestaurantViewModel extends AndroidViewModel {

    /*Partie Restaurant*/

    private RestaurantRepository repositoryRestaurant;
    private LiveData<List<Restaurant>> allRestaurants;
    private LiveData<Integer> nbRestaurants;

    /*Partie TypeRestaurant*/

    private TypeRestaurantRepository repositoryType;
    private LiveData<List<TypeRestaurant>> allTypes;

    /*Partie RestaurantTypeJoin*/
    private RestaurantTypeJoinRepository repositoryJoin;
    private LiveData<List<RestaurantTypeJoin>> allRestaurantTypes;

    public RestaurantViewModel(@NonNull Application application) {
        super(application);
        repositoryRestaurant = new RestaurantRepository(application);
        allRestaurants = repositoryRestaurant.getAllRestaurants();
        nbRestaurants = repositoryRestaurant.getNbRestaurants();

        repositoryType = new TypeRestaurantRepository(application);
        allTypes = repositoryType.getAllTypes();

        repositoryJoin = new RestaurantTypeJoinRepository(application);
        allRestaurantTypes = repositoryJoin.getAllRestaurantTypes();
    }

    /*Partie Restaurant*/
    public void insertRestaurant(Restaurant restaurant) {
        repositoryRestaurant.insertRestaurant(restaurant);
    }
    public LiveData<Long> getRestaurantId() {
        return repositoryRestaurant.getRestaurantId();
    }

    public LiveData<List<Restaurant>> getRestaurantById(int restaurantId){
        return repositoryRestaurant.getRestaurantById(restaurantId);
    }

    public void updateRestaurant(Restaurant restaurant) {
        repositoryRestaurant.updateRestaurant(restaurant);
    }
    public void deleteRestaurant(Restaurant restaurant) {
        repositoryRestaurant.deleteRestaurant(restaurant);
    }
    public void deleteAllRestaurants() {
        repositoryRestaurant.deleteAllRestaurants();
    }

    public LiveData<List<Restaurant>> getAllRestaurants() {
        return allRestaurants;
    }

    public LiveData<Integer> getNbRestaurants(){ return nbRestaurants; }

    public LiveData<Integer> getRestaurantIdByNameAddressDescription(String name, String address, String description){
        return repositoryRestaurant.getRestaurantIdByNameAddressDescription(name, address, description);
    }


    /*Partie TypeRestaurant*/

    /*public void insertType(TypeRestaurant type) {
        repositoryType.insertTypeRestaurant(type);
    }*/
    public void insertType(TypeRestaurant typeRestaurant) {
        repositoryType.insertTypeRestaurant(typeRestaurant);
    }
    public LiveData<Long> getTypeId() {
        return repositoryType.getTypeId();
    }

    /*public void updateType(TypeRestaurant type) {
        repositoryType.update(type);
    }*/

    /*public void deleteType(TypeRestaurant type) {
        repositoryType.delete(type);
    }*/

    public void deleteAllTypes() {
        repositoryType.deleteAllTypes();
    }

    public LiveData<String> getTypeById(int idType){
        return repositoryType.getTypeById(idType);
    }

    public LiveData<Integer> getIdByTypeName(String nomType){
        return repositoryType.getIdByTypeName(nomType);
    }

    public LiveData<List<TypeRestaurant>> getAllTypes() {
        return allTypes;
    }






    /*Partie RestaurantTypeJoin*/

    public void insert(RestaurantTypeJoin restaurantTypeJoin) {
        repositoryJoin.insertRestaurantType(restaurantTypeJoin);
    }

    public void deleteRestaurantTypeJoin(RestaurantTypeJoin restaurantTypeJoin) {
        repositoryJoin.deleteRestaurantTypeJoin(restaurantTypeJoin);
    }
    public LiveData<List<RestaurantTypeJoin>> getRestaurantTypes(int restaurantId) {
        return repositoryJoin.getRestaurantTypes(restaurantId);
    }

    public LiveData<List<RestaurantTypeJoin>> getAllRestaurantTypes() {
        return allRestaurantTypes;
    }

    public void deleteByRestaurantAndType(int restaurantId, int typeId) {
        repositoryJoin.deleteByRestaurantAndType(restaurantId, typeId);
    }

    public LiveData<Integer> getCount(int restaurantId, int typeId){
        return repositoryJoin.getCount(restaurantId, typeId);
    }

    public LiveData<Integer> getTypeIdByRestaurantId(int restaurantID){
        return repositoryJoin.getTypeIdByRestaurantId(restaurantID);
    }

    /*public boolean exists(int restaurantId, int typeId) {
        return repositoryJoin.exists(restaurantId, typeId);
    }*/
}
