package com.example.projetandroidrestoliste;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

public class RestaurantViewHolder extends RecyclerView.ViewHolder {

    private final TextView restaurantItemView;

    /*private final TextView adresseResto;

    private final TextView descriptionResto;*/



    static List<Restaurant> restaurantList;


    public RestaurantViewHolder(@NonNull View itemView) {
        super(itemView);

//        Log.d("currentList : ", String.valueOf(restaurantList));

        restaurantItemView = itemView.findViewById(R.id.textViewNom);
        /*adresseResto = itemView.findViewById(R.id.textViewAdresse);
        descriptionResto = itemView.findViewById(R.id.textViewDescription);*/



        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = getAdapterPosition();

                //Log.d("position item : ", String.valueOf(position));

//                Restaurant currentResto = restaurantList.get(position);

                Restaurant currentResto = RestaurantViewHolder.restaurantList.get(position);
                Intent intent = new Intent(v.getContext(), Consultation.class);
                intent.putExtra("nomResto", currentResto.getNomResto());
                intent.putExtra("adresseResto", currentResto.getAdresseResto());
                intent.putExtra("descriptionResto", currentResto.getDescriptionResto());
                v.getContext().startActivity(intent);
            }
        });
    }

    public void bind(String text, String text2, String text3){
        restaurantItemView.setText(text);
        /*adresseResto.setText(text2);
        descriptionResto.setText(text3);*/
    }

    static RestaurantViewHolder create(ViewGroup parent){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
        return new RestaurantViewHolder(view);
    }
}
