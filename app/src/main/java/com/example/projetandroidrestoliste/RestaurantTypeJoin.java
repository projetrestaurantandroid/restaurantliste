package com.example.projetandroidrestoliste;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "restaurant_type_join", primaryKeys = {"restaurantId", "typeId"} /*, indices = {@Index("restaurantId"), @Index("typeId")}*/)
public class RestaurantTypeJoin {

    @ColumnInfo(name = "restaurantId")
    public int restaurantId;

    @ColumnInfo(name = "typeId")
    public int typeId;

    public RestaurantTypeJoin(int restaurantId, int typeId) {
        this.restaurantId = restaurantId;
        this.typeId = typeId;
    }



    public int getRestaurantId() {
        return restaurantId;
    }

    public int getTypeId() {
        return typeId;
    }



    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }
}


