package com.example.projetandroidrestoliste;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

public class TypeRestaurantRepository {

    private TypeRestaurantDao typeRestaurantDao;
    private LiveData<List<TypeRestaurant>> allTypes;

    public TypeRestaurantRepository(Application application) {
        RestaurantRoomDatabase db = RestaurantRoomDatabase.getDatabase(application);
        typeRestaurantDao = db.typeRestaurantDao();
        allTypes = typeRestaurantDao.getAllTypes();
    }
    public LiveData<List<TypeRestaurant>> getAllTypes() {
        return allTypes;
    }

    public LiveData<String> getTypeById(int idType){
        return typeRestaurantDao.getTypeById(idType);
    }

    public LiveData<Integer> getIdByTypeName(String nomType){
        return typeRestaurantDao.getIdByTypeName(nomType);
    }

    private static MutableLiveData<Long> typeId = new MutableLiveData<>();
    public MutableLiveData<Long> getTypeId(){
        return typeId;
    }
    public void insertTypeRestaurant(TypeRestaurant typeRestaurant) {
        new InsertTypeRestaurantAsyncTask(typeRestaurantDao).execute(typeRestaurant);
    }

    public void deleteAllTypes() {
        new DeleteAllTypesAsyncTask(typeRestaurantDao).execute();
    }


    private static class InsertTypeRestaurantAsyncTask extends AsyncTask<TypeRestaurant, Void, Long> {
        private TypeRestaurantDao typeRestaurantDao;

        private InsertTypeRestaurantAsyncTask(TypeRestaurantDao typeRestaurantDao) {
            this.typeRestaurantDao = typeRestaurantDao;
        }

        @Override
        protected Long doInBackground(TypeRestaurant... types) {
            long id = typeRestaurantDao.insertType(types[0]);
            return id;
        }

        @Override
        protected void onPostExecute(Long id) {
            super.onPostExecute(id);
            typeId.postValue(id);
        }
    }

    private static class DeleteTypeAsyncTask extends AsyncTask<TypeRestaurant, Void, Void> {
        private TypeRestaurantDao typeRestaurantDao;

        private DeleteTypeAsyncTask(TypeRestaurantDao typeRestaurantDao) {
            this.typeRestaurantDao = typeRestaurantDao;
        }

        @Override
        protected Void doInBackground(TypeRestaurant... typeRestaurants) {
            typeRestaurantDao.deleteType(typeRestaurants[0]);
            return null;
        }
    }

    private static class DeleteAllTypesAsyncTask extends AsyncTask<Void, Void, Void> {
        private TypeRestaurantDao typeRestaurantDao;

        private DeleteAllTypesAsyncTask(TypeRestaurantDao typeRestaurantDao) {
            this.typeRestaurantDao = typeRestaurantDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            typeRestaurantDao.deleteAllTypes();
            return null;
        }
    }
}
