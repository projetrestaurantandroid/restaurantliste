package com.example.projetandroidrestoliste;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import java.util.List;

@Entity(tableName = "table_restaurant", indices = {@Index(value = {"nom"}, unique = true)})
public class Restaurant {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "nom")
    private String nomResto;
    @ColumnInfo(name = "adresse")
    private String adresseResto;
    @ColumnInfo(name = "description")
    private String descriptionResto;

    //@Relation(parentColumn = "id", entityColumn = "restaurantId", entity = RestaurantTypeJoin.class)

    //private List<TypeRestaurant> types;

    public Restaurant(String nomResto, String adresseResto, String descriptionResto){
        this.nomResto = nomResto;
        this.adresseResto = adresseResto;
        this.descriptionResto = descriptionResto;
    }
    public int getId() {
        return this.id;
    }
    public String getNomResto() {
        return this.nomResto;
    }
    public String getAdresseResto() {
        return this.adresseResto;
    }
    public String getDescriptionResto() {
        return this.descriptionResto;
    }
    /*public List<TypeRestaurant> getTypes() {
        return this.types;
    }*/
    public void setId(int id) {
        this.id = id;
    }


   /* public void setTypes(List<TypeRestaurant> types) {
        this.types = types;
    }*/
}



//    @ForeignKey(entity = RestaurantTypeJoin.class, parentColumns = "restaurantId", childColumns = "id")



