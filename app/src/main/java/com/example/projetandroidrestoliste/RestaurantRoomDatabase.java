package com.example.projetandroidrestoliste;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.ForeignKey;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Restaurant.class, TypeRestaurant.class, RestaurantTypeJoin.class}, version = 1, exportSchema = false)
public abstract class RestaurantRoomDatabase extends RoomDatabase {

    public abstract RestaurantDao restaurantDao();
    public abstract  TypeRestaurantDao typeRestaurantDao();
    public abstract  RestaurantTypeJoinDao restaurantTypeJoinDao();

    private static volatile RestaurantRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;

    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static RestaurantRoomDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (RestaurantRoomDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), RestaurantRoomDatabase.class, "restaurant_database").addCallback(sRoomDatabaseCallback).build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            // If you want to keep data through app restarts,
            // comment out the following block
            databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                // If you want to start with more words, just add them.
                /*RestaurantDao dao = INSTANCE.restaurantDao();
                dao.deleteAllRestaurants();



                Restaurant restaurant = new Restaurant("Au Joyeux Codeur", "Place du code", "Un sacré plaisir d'y être allé.");
                dao.insertRestaurant(restaurant);
                restaurant = new Restaurant("Buvons Java", "Rue de l'Android", "Un petit plaisir.");
                dao.insertRestaurant(restaurant);*/


                TypeRestaurantDao typeRestaurantDao = INSTANCE.typeRestaurantDao();
                typeRestaurantDao.deleteAllTypes();

                TypeRestaurant typeRestaurant = new TypeRestaurant("fastfood");
                typeRestaurantDao.insertType(typeRestaurant);
                typeRestaurant = new TypeRestaurant("gastronomique");
                typeRestaurantDao.insertType(typeRestaurant);
                typeRestaurant = new TypeRestaurant("pizzeria");
                typeRestaurantDao.insertType(typeRestaurant);
                typeRestaurant = new TypeRestaurant("grill");
                typeRestaurantDao.insertType(typeRestaurant);
                typeRestaurant = new TypeRestaurant("buffet à volonté");
                typeRestaurantDao.insertType(typeRestaurant);
                typeRestaurant = new TypeRestaurant("pas de catégorie");
                typeRestaurantDao.insertType(typeRestaurant);

            });
        }
    };
}
