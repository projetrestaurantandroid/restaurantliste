package com.example.projetandroidrestoliste;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class RestaurantTypeJoinRepository {

    private RestaurantTypeJoinDao restaurantTypeJoinDao;
    private LiveData<List<RestaurantTypeJoin>> allRestaurantTypes;

    public RestaurantTypeJoinRepository(Application application) {
        RestaurantRoomDatabase database = RestaurantRoomDatabase.getDatabase(application);
        restaurantTypeJoinDao = database.restaurantTypeJoinDao();
        allRestaurantTypes = restaurantTypeJoinDao.getAllRestaurantTypes();
    }

    /*public boolean exists(int restaurantId, int typeId){
        return restaurantTypeJoinDao.exists(restaurantId, typeId) != null;
    }*/

    public void insertRestaurantType(RestaurantTypeJoin restaurantTypeJoin) {
        new InsertRestaurantTypeJoinAsyncTask(restaurantTypeJoinDao).execute(restaurantTypeJoin);
    }

    public void deleteRestaurantTypeJoin(RestaurantTypeJoin restaurantTypeJoin) {
        new DeleteJoinAsyncTask(restaurantTypeJoinDao).execute(restaurantTypeJoin);
    }

    public LiveData<List<RestaurantTypeJoin>> getAllRestaurantTypes(){
        return allRestaurantTypes;
    }

    public LiveData<List<RestaurantTypeJoin>> getRestaurantTypes(int restaurantId) {
        return restaurantTypeJoinDao.getRestaurantTypes(restaurantId);
    }

    public LiveData<Integer> getCount(int restaurantId, int typeId){
        return restaurantTypeJoinDao.getCount(restaurantId, typeId);
    }


    public LiveData<Integer> getTypeIdByRestaurantId(int restaurantID){
        return restaurantTypeJoinDao.getTypeIdByRestaurantId(restaurantID);
    }


    public void deleteByRestaurantAndType(int restaurantId, int typeId) {
        new DeleteRestaurantTypeJoinAsyncTask(restaurantTypeJoinDao).execute(restaurantId, typeId);
    }

    private static class InsertRestaurantTypeJoinAsyncTask extends AsyncTask<RestaurantTypeJoin, Void, Void> {
        private RestaurantTypeJoinDao restaurantTypeJoinDao;

        private InsertRestaurantTypeJoinAsyncTask(RestaurantTypeJoinDao restaurantTypeJoinDao) {
            this.restaurantTypeJoinDao = restaurantTypeJoinDao;
        }

        @Override
        protected Void doInBackground(RestaurantTypeJoin... restaurantTypeJoins) {
            restaurantTypeJoinDao.insertRestaurantType(restaurantTypeJoins[0]);
            return null;
        }
    }

    private static class DeleteRestaurantTypeJoinAsyncTask extends AsyncTask<Integer, Void, Void> {
        private RestaurantTypeJoinDao restaurantTypeJoinDao;

        private DeleteRestaurantTypeJoinAsyncTask(RestaurantTypeJoinDao restaurantTypeJoinDao) {
            this.restaurantTypeJoinDao = restaurantTypeJoinDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            restaurantTypeJoinDao.deleteByRestaurantAndType(integers[0], integers[1]);
            return null;
        }
    }

    private static class DeleteJoinAsyncTask extends AsyncTask<RestaurantTypeJoin, Void, Void> {
        private RestaurantTypeJoinDao restaurantTypeJoinDao;

        private DeleteJoinAsyncTask(RestaurantTypeJoinDao restaurantTypeJoinDao) {
            this.restaurantTypeJoinDao = restaurantTypeJoinDao;
        }

        @Override
        protected Void doInBackground(RestaurantTypeJoin... restaurantTypeJoins) {
            restaurantTypeJoinDao.deleteRestaurantTypeJoin(restaurantTypeJoins[0]);
            return null;
        }
    }
}
