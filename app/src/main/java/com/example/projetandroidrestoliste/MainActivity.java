package com.example.projetandroidrestoliste;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity{

    public static final int NEW_RESTAURANT_ACTIVITY_REQUEST_CODE = 1;

    private RestaurantViewModel restaurantViewModel;


    private Observer<Long> restaurantIdObserver;
    private Observer<Long> typeIdObserver;
    private Observer<Integer> countObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);

        final RestaurantListAdapter adapter = new RestaurantListAdapter(new RestaurantListAdapter.RestaurantDiff());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //associe le view model à l'activité
        restaurantViewModel = new ViewModelProvider(this).get(RestaurantViewModel.class);

        restaurantViewModel.getAllRestaurants().observe(this, restaurants -> {
            adapter.submitList(restaurants);
        });

        restaurantViewModel.getNbRestaurants().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                TextView textView = findViewById(R.id.tvNbRestaurants);
                textView.setText("Nombre de restaurants : "+integer);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener( view -> {
            Intent intent = new Intent(MainActivity.this, new_restaurant.class);
            startActivityForResult(intent, NEW_RESTAURANT_ACTIVITY_REQUEST_CODE);
        });

        // Create a button for deleting all items in the database
        FloatingActionButton deleteAllButton = findViewById(R.id.deleteAll);

        // Set an onClickListener for the button
        deleteAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Call the deleteAll method on the RestaurantViewModel
                restaurantViewModel.deleteAllRestaurants();
                restaurantViewModel.deleteAllTypes();
            }
        });


    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_RESTAURANT_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            String restaurantNom = data.getStringExtra(new_restaurant.EXTRA_REPLY);
            String restaurantAdresse = data.getStringExtra(new_restaurant.EXTRA_REPLY_ADRESSE);
            String restaurantDescription = data.getStringExtra(new_restaurant.EXTRA_REPLY_DESCRIPTION);

            //récupère le catégories associées à mon restaurant
            ArrayList<String> listCatChecked = data.getStringArrayListExtra(new_restaurant.EXTRA_REPLY_LISTCATEGORIE);

            Log.d("SORTIE", restaurantNom);
            Log.d("SORTIE", restaurantAdresse);
            Log.d("SORTIE", restaurantDescription);
            Log.d("ListCatCheckedInMain", String.valueOf(listCatChecked));



            Restaurant restaurant = new Restaurant(restaurantNom, restaurantAdresse, restaurantDescription);
            restaurantViewModel.insertRestaurant(restaurant);

            restaurantViewModel.getRestaurantId().observe(this, new Observer<Long>() {

                LiveData<Integer> monId;

                @Override
                public void onChanged(Long aLong) {
                    Log.d("SEPARATOR", "-----------------------");
                    Log.d("IDRESTO", String.valueOf(aLong));
                    //restaurantTypeJoin.setRestaurantId(Math.toIntExact(aLong));
                    //Log.d("RESTAURANTIDJOIN", String.valueOf(restaurantTypeJoin.getRestaurantId()));


                    if(listCatChecked.isEmpty()){
                        monId = restaurantViewModel.getIdByTypeName("pas de catégorie");
                        monId.observe(MainActivity.this, new Observer<Integer>() {
                            @Override
                            public void onChanged(Integer integer) {
                                Log.d("SEPARATOR", "-----------------------");
                                Log.d("RETURN", String.valueOf(integer));

                                //restaurantTypeJoin.setTypeId(integer);



                                RestaurantTypeJoin restaurantTypeJoin = new RestaurantTypeJoin(Math.toIntExact(aLong), integer);

                                Log.d("RTJRID", String.valueOf(restaurantTypeJoin.getRestaurantId()));
                                Log.d("RTJTID", String.valueOf(restaurantTypeJoin.getTypeId()));

                                restaurantViewModel.insert(restaurantTypeJoin);
                            }
                        });
                    }
                    else{
                        Log.d("SIZELIST", String.valueOf(listCatChecked.size()));
                        for (int i = 0; i < listCatChecked.size(); i++) {

                            monId = restaurantViewModel.getIdByTypeName(listCatChecked.get(i));
                            int finalI = i;
                            monId.observe(MainActivity.this, new Observer<Integer>() {
                                @Override
                                public void onChanged(Integer integer) {
                                    Log.d("SEPARATOR", "-----------------------");
                                    Log.d("RETURN", String.valueOf(listCatChecked.get(finalI)) + " -> " +String.valueOf(integer));

                                    //restaurantTypeJoin.setTypeId(integer);



                                    RestaurantTypeJoin restaurantTypeJoin = new RestaurantTypeJoin(Math.toIntExact(aLong), integer);

                                    Log.d("RTJRID", String.valueOf(restaurantTypeJoin.getRestaurantId()));
                                    Log.d("RTJTID", String.valueOf(restaurantTypeJoin.getTypeId()));

                                    restaurantViewModel.insert(restaurantTypeJoin);

                                }
                            });
                        }
                    }
                }
            });


            /*restaurantIdObserver = new Observer<Long>() {
                @Override
                public void onChanged(Long aLong) {
                    Log.d("IDRESTO", String.valueOf(aLong));
                    restaurantTypeJoin.setRestaurantId(Math.toIntExact(aLong));
                    Log.d("RESTAURANTIDJOIN", String.valueOf(restaurantTypeJoin.getRestaurantId()));

                    restaurantViewModel.getTypeId().removeObserver(typeIdObserver);
                    restaurantViewModel.getTypeId().observe(MainActivity.this, typeIdObserver);

                }
            };

            typeIdObserver = new Observer<Long>() {
                @Override
                public void onChanged(Long aLong) {
                    Log.d("IDTYPE", String.valueOf(aLong));
                    restaurantTypeJoin.setTypeId(Math.toIntExact(aLong));
                    Log.d("TYPEIDJOIN", String.valueOf(restaurantTypeJoin.getTypeId()));

                    restaurantViewModel.getCount(restaurantTypeJoin.getRestaurantId(), restaurantTypeJoin.getTypeId()).removeObserver(countObserver);
                    restaurantViewModel.getCount(restaurantTypeJoin.getRestaurantId(), restaurantTypeJoin.getTypeId()).observe(MainActivity.this, countObserver);
                }
            };

            countObserver = new Observer<Integer>() {
                @Override
                public void onChanged(Integer integer) {
                    Log.d("COUNT", String.valueOf(integer));
                    if(integer == 0){
                        Log.d("SAVE", "enregistrement effectué");
                        restaurantViewModel.insert(restaurantTypeJoin);
                        restaurantViewModel.getRestaurantId().removeObserver(restaurantIdObserver);
                        restaurantViewModel.getTypeId().removeObserver(typeIdObserver);
                        restaurantViewModel.getCount(restaurantTypeJoin.getRestaurantId(), restaurantTypeJoin.getTypeId()).removeObserver(countObserver);
                    }
                }
            };

            restaurantViewModel.getRestaurantId().observe(this, restaurantIdObserver);*/


        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }
}