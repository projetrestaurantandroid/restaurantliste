package com.example.projetandroidrestoliste;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import java.util.ArrayList;
import java.util.List;

public class RestaurantListAdapter extends ListAdapter<Restaurant, RestaurantViewHolder> {


    protected RestaurantListAdapter(@NonNull DiffUtil.ItemCallback<Restaurant> diffCallback) {
        super(diffCallback);
    }

    protected RestaurantListAdapter(@NonNull AsyncDifferConfig<Restaurant> config) {
        super(config);
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return RestaurantViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
        Restaurant current = getItem(position);
        holder.bind(current.getNomResto(), current.getAdresseResto(), current.getDescriptionResto());
        holder.itemView.setTag(current.getId());
        RestaurantViewHolder.restaurantList = getCurrentList(); //ajout de cette ligne
    }



    static class RestaurantDiff extends DiffUtil.ItemCallback<Restaurant>{

        @Override
        public boolean areItemsTheSame(@NonNull Restaurant oldItem, @NonNull Restaurant newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Restaurant oldItem, @NonNull Restaurant newItem) {
            return oldItem.getNomResto().equals(newItem.getNomResto());
        }
    }
}
