package com.example.projetandroidrestoliste;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import java.util.List;

@Entity(tableName = "table_typeRestaurant")
public class TypeRestaurant {
    @PrimaryKey(autoGenerate = true)
    public int idType;
    @ColumnInfo(name = "type")
    private String typeRestaurant;
    public TypeRestaurant(String typeRestaurant){
        this.typeRestaurant = typeRestaurant;
    }
    public int getIdType() {
        return idType;
    }
    public String getTypeRestaurant() {
        return typeRestaurant;
    }
    public void setIdType(int idType) {
        this.idType = idType;
    }
}
//    @ForeignKey(entity = RestaurantTypeJoin.class, parentColumns = "typeId", childColumns = "idType")