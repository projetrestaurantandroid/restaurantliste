package com.example.projetandroidrestoliste;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface RestaurantDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertRestaurant(Restaurant restaurant);

    @Update
    void updateRestaurant(Restaurant restaurant);

    @Delete
    void deleteRestaurant(Restaurant restaurant);

    @Query("DELETE FROM table_restaurant")
    void deleteAllRestaurants();

    @Query("SELECT * FROM table_restaurant")
    LiveData<List<Restaurant>> getAllRestaurants();

    @Query("SELECT * FROM table_restaurant WHERE id = :restaurantId")
    LiveData<List<Restaurant>> getRestaurantById(int restaurantId);

    @Query("SELECT id FROM table_restaurant WHERE nom = :name AND adresse = :address AND description = :description")
    LiveData<Integer> getRestaurantIdByNameAddressDescription(String name, String address, String description);

    @Query("SELECT count(*) from table_restaurant")
    LiveData<Integer> getNbRestaurant();
}
